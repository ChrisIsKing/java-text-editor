package JavaTextEditor.Models;

import java.io.File;
/**
 * Created by ChrisIsKing on 4/27/16.
 */
public class TextDocument {

    private String fileName = null;
    private File fileRef = null;
    private String content = null;


    public TextDocument(String fileName, String content) {
        this.fileName = fileName;
        this.content = content;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileRef(File fileRef) {
        //also set the fileName from this
        fileName = fileRef.getName();
        this.fileRef = fileRef;
    }

    public File getFileRef() {
        return fileRef;
    }

    public String getContents() {
        return content;
    }

    public void setContents(String content) {
        this.content = content;
    }
}
