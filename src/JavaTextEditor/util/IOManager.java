package JavaTextEditor.util;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileWriter;

/**
 * Created by ChrisIsKing on 4/27/16.
 */
public class IOManager {

    private double filesizeLimit = 1048576;

    public IOManager() {
        setFilesizeLimitInMB(2);
    }


    public int getFilesizeLimitInMB() {
        return (int) filesizeLimit/1048576;
    }

    public double getFilesizeLimit() {
        return filesizeLimit;
    }

    public void setFilesizeLimitInMB(int limitInMb) {
        filesizeLimit = (limitInMb * 1048576);
    }

    public String readTextFile(File f) throws IOException {
        FileReader in = null;
        String contents = null;

        in = new FileReader(f);
        int size = (int)f.length();
        if(getFilesizeLimit() < size) {
            throw new OutOfMemoryError("File exceeds filesize limit of "+getFilesizeLimitInMB()+"MB");
        }

        char[] data = new char[size];

        in.read(data, 0, size);
        contents = new String(data);
        if (in != null) {
            in.close();
        }

        return contents;

    }

    public void writeTextFile(File f, String fileData) throws IOException {

        BufferedWriter buffout = null;

        char[] charsOut = fileData.toCharArray();
        buffout = new BufferedWriter(new FileWriter(f));

        buffout.write(charsOut, 0, charsOut.length);

        if(buffout!=null) {
            buffout.flush();
            buffout.close();
        }
    }

}
