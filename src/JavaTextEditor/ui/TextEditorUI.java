package JavaTextEditor.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import JavaTextEditor.JavaTextEditorController;
import JavaTextEditor.Models.TextDocument;

/**
 * Created by ChrisIsKing on 4/26/16.
 */
public class TextEditorUI extends JFrame {

    //private  JTextArea textArea;
    private JPopupMenu popup;
    private JTabbedPane tabbedPane;
    private HelpDialog helpDialog;
    private JFileChooser fileChooser;
    private JavaTextEditorController controller;
    public TextEditorUI() {

        controller = new JavaTextEditorController(this);

        //instantiate the help dialog..we need to catch any exceptions since we are reading
        //from an external file.
        try {
            helpDialog = new HelpDialog();
        }catch(MalformedURLException ex) {
            showErrorDialog("Incorrect path!");
        }
        catch(IOException ioe) {
            showErrorDialog("Unable to read file");
        }

        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new TextFileFilter());

        // set up the JMenuBar
        JMenuBar menuBar = new JMenuBar();
        //add it to the frame
        setJMenuBar(menuBar);

        TextEditorEventListener editorListener = new TextEditorEventListener();

        //build the file menu and add it to the frame
        JMenu fileMenu = new JMenu("File");
        //set mnemonic for menu
        fileMenu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(fileMenu);
        //build and add the menuItem of the file menu
        JMenuItem newMenuItem = new JMenuItem("New", new ImageIcon("images/new.gif"));
        //add listener to trigger spawning of new tab when clicked
        newMenuItem.addActionListener(editorListener);
        fileMenu.add(newMenuItem);
        //add accelerator to menu item
        newMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        JMenuItem openMenuItem = new JMenuItem("Open", new ImageIcon("images/open.gif"));
        openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        openMenuItem.addActionListener(editorListener);
        fileMenu.add(openMenuItem);
        JMenuItem closeMenuItem = new JMenuItem("Close", new ImageIcon("images/close.gif"));
        closeMenuItem.addActionListener(editorListener);
        fileMenu.add(closeMenuItem);
        //add a separator
        fileMenu.addSeparator();
        JMenuItem saveMenuItem = new JMenuItem("Save", new ImageIcon("images/save.gif"));
        saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        saveMenuItem.addActionListener(editorListener);
        fileMenu.add(saveMenuItem);
        JMenuItem saveAsMenuItem = new JMenuItem("Save As", new ImageIcon("images/save_as.gif"));
        fileMenu.add(saveAsMenuItem);
        //add a separator
        fileMenu.addSeparator();
        JMenuItem exitMenuItem = new JMenuItem("Exit", new ImageIcon("images/exit.gif"));
        fileMenu.add(exitMenuItem);


        //build the edit menu and add it to the frame
        JMenu editMenu = new JMenu("Edit");
        //set mnemonic for edit
        editMenu.setMnemonic(KeyEvent.VK_E);
        menuBar.add(editMenu);
        //build and add the menuItem of the edit menu
        JMenuItem undoMenuItem = new JMenuItem("Undo", new ImageIcon("images/undo.gif"));
        undoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
        editMenu.add(undoMenuItem);
        JMenuItem redoMenuItem = new JMenuItem("Redo", new ImageIcon("images/redo.gif"));
        redoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
        editMenu.add(redoMenuItem);
        //add a separator
        editMenu.addSeparator();
        JMenuItem cutMenuItem = new JMenuItem("Cut", new ImageIcon("images/cut.gif"));
        cutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        editMenu.add(cutMenuItem);
        JMenuItem copyMenuItem = new JMenuItem("Copy", new ImageIcon("images/copy.gif"));
        copyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        editMenu.add(copyMenuItem);
        JMenuItem pasteMenuItem = new JMenuItem("Paste", new ImageIcon("images/paste.gif"));
        pasteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
        editMenu.add(pasteMenuItem);
        editMenu.addSeparator();
        JMenuItem selectAllMenuItem = new JMenuItem("Select All", new ImageIcon("images/sel_all.gif"));
        selectAllMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
        editMenu.add(selectAllMenuItem);

        //build the format menu and add it to the frame
        JMenu formatMenu = new JMenu("Format");
        //set mnemonic for format menu
        formatMenu.setMnemonic(KeyEvent.VK_O);
        menuBar.add(formatMenu);
        //build and add the menuItem of the format menu
        JCheckBoxMenuItem formatMenuItem = new JCheckBoxMenuItem("Wrap text");
        formatMenu.add(formatMenuItem);
        //build the text size submenu and add it to the format menu
        JMenu textSizeMenu = new JMenu("Text Size");
        formatMenu.add(textSizeMenu);

        //add radio button menu items to select text sizes..
        //add a buttongroup so that only one radio can be selected at a time
        ButtonGroup buttonGroup = new ButtonGroup();
        JRadioButtonMenuItem smallTextOption = new JRadioButtonMenuItem("Small");
        buttonGroup.add(smallTextOption);
        textSizeMenu.add(smallTextOption);
        JRadioButtonMenuItem mediumTextOption = new JRadioButtonMenuItem("Medium");
        buttonGroup.add(mediumTextOption);
        textSizeMenu.add(mediumTextOption);
        //set medium as selected
        mediumTextOption.setSelected(true);
        JRadioButtonMenuItem largeTextOption = new JRadioButtonMenuItem("Large");
        buttonGroup.add(largeTextOption);
        textSizeMenu.add(largeTextOption);


        //set up and add the textArea in a scrollpane
        //textArea = new JTextArea();
        //add(new JScrollPane(textArea), BorderLayout.CENTER);
        //set up the JTabbedPane
        tabbedPane = new JTabbedPane();
        //add it to the frame..
        add(tabbedPane, BorderLayout.CENTER);

        //set up a pop-up menu on the textArea
        popup = new JPopupMenu();
        //add cut menu item to popup
        JMenuItem cutPopupMenuItem = new JMenuItem("Cut", new ImageIcon("images/cut.gif"));
        popup.add(cutPopupMenuItem);
        //add copy menu item to popup
        JMenuItem copyPopupMenuItem = new JMenuItem("Copy", new ImageIcon("images/copy.gif"));
        popup.add(copyPopupMenuItem);
        //add paste menu item to popup
        JMenuItem pastePopupMenuItem = new JMenuItem("Paste", new ImageIcon("images/paste.gif"));
        popup.add(pastePopupMenuItem);
        //add separator on popup
        popup.addSeparator();
        //add select all menu item to popup
        JMenuItem selectAllPopupMenuItem = new JMenuItem("Select All", new ImageIcon("images/sel_all.gif"));
        popup.add(selectAllPopupMenuItem);

        //register mouse listener on the textArea to show popup on right click
        /*textArea.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    popup.show(textArea, e.getX(), e.getY());
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    popup.show(textArea, e.getX(), e.getY());
                }
            }
        });*/

        //set up the toolbar with items
        JToolBar toolBar = new JToolBar();
        //add tool bar to frame..
        add(toolBar, BorderLayout.NORTH);
        //add toolbar button
        JButton newButton = new JButton(new ImageIcon("images/new.gif"));
        //add listener to button
        newButton.addActionListener(editorListener);
        newButton.setToolTipText("new document");
        toolBar.add(newButton);
        JButton openButton = new JButton(new ImageIcon("images/open.gif"));
        openButton.setToolTipText("open document");
        //add listener to button
        openButton.addActionListener(editorListener);
        toolBar.add(openButton);
        JButton saveButton = new JButton(new ImageIcon("images/save.gif"));
        saveButton.addActionListener(editorListener);
        saveButton.setToolTipText("save document");
        toolBar.add(saveButton);
        //add separator
        toolBar.addSeparator();
        JButton undoButton = new JButton(new ImageIcon("images/undo.gif"));
        undoButton.setToolTipText("undo change");
        toolBar.add(undoButton);
        JButton redoButton = new JButton(new ImageIcon("images/redo.gif"));
        redoButton.setToolTipText("redo changes");
        toolBar.add(redoButton);
        toolBar.addSeparator();
        JButton cutButton = new JButton(new ImageIcon("images/cut.gif"));
        cutButton.setToolTipText("cut text");
        toolBar.add(cutButton);
        JButton copyButton = new JButton(new ImageIcon("images/copy.gif"));
        copyButton.setToolTipText("copy text");
        toolBar.add(copyButton);
        JButton pasteButton = new JButton(new ImageIcon("images/paste.gif"));
        pasteButton.setToolTipText("paste text");
        toolBar.add(pasteButton);
        JButton selectAllButton = new JButton(new ImageIcon("images/sel_all.gif"));
        selectAllButton.setToolTipText("select all");
        toolBar.add(selectAllButton);

        //build the help menu and add it to the frame
        JMenu helpMenu = new JMenu("Help");
        //add it to the menubar
        menuBar.add(helpMenu);
        //build and add the menuItem of the help menu
        JMenuItem documentationMenuItem = new JMenuItem("Documentation");
        //add a listener on the documentation menu item so it can trigger the HelpDialog
        documentationMenuItem.addActionListener(editorListener);
        helpMenu.add(documentationMenuItem);
        //build and add the menuItem of the help menu
        JMenuItem aboutMenuItem = new JMenuItem("About");
        helpMenu.add(aboutMenuItem);


        // add first tab by default
        addTextEditorTab(new TextDocument("Untitled", ""));

        //add some initializations for this frame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(700,500);
        setTitle("New Document");
        setVisible(true);
    }

    public final void addTextEditorTab(TextDocument document) {

        //create TextEditorArea
        final TextEditorArea textEditor = new TextEditorArea(document);
        //add the mouse listener to the text editor area for the pop up menu...
        //register mouse listener on the textArea to show popup on right click
        textEditor.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if(e.isPopupTrigger()) {
                    popup.show(textEditor, e.getX(), e.getY());
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(e.isPopupTrigger()) {
                    popup.show(textEditor, e.getX(), e.getY());
                }
            }
        });

        //add this to the tabbed pane in a scrollpane
        tabbedPane.addTab(document.getFileName(), new JScrollPane(textEditor));

        //make sure you set the newly added tab as active so that it shows on top..
        tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
    }

    //closes the text editor tab that is currently active/visible
    public void removeSelectedTextEditorTab() {
        //we may not have any tabs..return if this is the case..
        if(tabbedPane.getTabCount() == 0) return;

        //get the tab that is active from the tabbedPane
        int selectedTabIndex = tabbedPane.getSelectedIndex();
        //remove it
        tabbedPane.removeTabAt(selectedTabIndex);
    }

    //create a subclass listener to interpret events
    private class TextEditorEventListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent evt) {
            //try to interpret the button label or tooltip text to trigger actions

            //grab what's triggering the action...
            Object source = evt.getSource();

            if( source instanceof JMenuItem ) {
                //menu items have labels, we can get and interpret thru the getActionCommand()
                switch(evt.getActionCommand()) {
                    case "New":
                        addTextEditorTab(new TextDocument("Untitled", ""));
                        break;
                    case "Open":
                        controller.handleOpenTextDocument();
                        break;
                    case "Save":
                        controller.handleSaveTextDocument();
                        break;

                    case "Close":
                        removeSelectedTextEditorTab();
                        break;
                    case "Documentation":
                        //show help dialog
                        helpDialog.showWindow();
                        break;
                }
            }
            else if( source instanceof JButton ) {
                //the buttons we used just have icons..but they have tool tips we can use to interpret..
                switch( ((JButton)evt.getSource()).getToolTipText() ) {
                    case "new document":
                        addTextEditorTab(new TextDocument("Untitled", ""));
                        break;
                    case "open document":
                        controller.handleOpenTextDocument();
                        break;
                    case "save document":
                        controller.handleSaveTextDocument();
                        break;
                }
            }

        }
    }

    public File showOpenFileDialog() {
        int option = fileChooser.showOpenDialog(this);
        if(option == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile();
        } else {
            return null;
        }
    }

    public File showSaveFileDialog() {
        int option = fileChooser.showSaveDialog(this);
        if(option == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile();
        } else {
            return null;
        }
    }

    //allows the renaming of the currently selected tab...
    //useful for when a new document is saved or an existing
    //document is saved as.. and the file name changes..
    public void setActiveTabTitle(String title) {
        tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(), title);
    }

    public TextEditorArea getSelectedTextArea() {
        //first we grab the entire component at the selected tab...
        //which is the instance of the JScrollPane that scrolls the text editor area
        JScrollPane scrollPane = (JScrollPane)tabbedPane.getSelectedComponent();
        //now we need to ask the scrollpane (nicely) for our TextEditorArea..
        TextEditorArea textEditorArea = (TextEditorArea)((scrollPane.getViewport()).getView());
        //once we have this object..we return it
        return textEditorArea;
    }


    public final void showErrorDialog(String error) {
        JOptionPane.showMessageDialog(this, error,"ERROR - ImageViewer", JOptionPane.ERROR_MESSAGE);
    }
}
