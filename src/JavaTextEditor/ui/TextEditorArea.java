package JavaTextEditor.ui;

import javax.swing.*;
import JavaTextEditor.Models.TextDocument;

/**
 * Created by ChrisIsKing on 4/27/16.
 */
public class TextEditorArea extends JTextArea {

    private TextDocument document;

    public TextEditorArea() {
        super();
        document = new TextDocument("untitled.txt","");
    }

    public TextEditorArea(TextDocument document) {
        super(document.getContents());

        this.document = document;
    }

    public TextDocument getTextDocument() {
        //return the TextDocument currently dsplayed in this
        //TextEditorArea...make sure you update its contents before you return it
        //to get the latest changes made by the user
        document.setContents(this.getText());
        return document;
    }

    public void setTextDocument(TextDocument document) {
        this.document = document;
        //grab the contents from the TextDocument object and
        //display them in the TextEditorArea
        setText(document.getContents());
    }
}
