package JavaTextEditor.ui;

import javax.swing.*;
import java.net.*;
import java.io.*;
import java.awt.Dimension;
/**
 * Created by ChrisIsKing on 4/27/16.
 */
public class HelpDialog extends JFrame {

    private URL resourceURL = null;

    public HelpDialog() throws MalformedURLException, IOException {
        JEditorPane editorPane = new JEditorPane();
        //dynamically construct path to the help file
        //resourceURL = new URL();
        resourceURL = new URL("file:"
                + System.getProperty("user.dir")
                + System.getProperty("file.separator")
                +"docs"
                + System.getProperty("file.separator")
                + "JavaTextEditorHelp.html");
        //set the path for the html file to be read into the editor pane
        editorPane.setEditable(false);
        editorPane.setPage(resourceURL);

        JScrollPane editorScrollPane = new JScrollPane(editorPane);
        editorScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setPreferredSize(new Dimension(500, 500));

        add(editorScrollPane);

        //editorPane.setVisible(true);
        editorPane.setSize(500,500);

        setSize(520, 540);
        setResizable(false);
    }

    public void showWindow() {
        setVisible(true);
    }

    public void hideWindow() {
        setVisible(false);
    }

}
