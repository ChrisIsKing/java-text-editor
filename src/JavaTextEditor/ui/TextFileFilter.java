package JavaTextEditor.ui;

import java.io.File;

/**
 * Created by ChrisIsKing on 4/27/16.
 */
public class TextFileFilter extends javax.swing.filechooser.FileFilter {
    private final String[] okFileExtensions = new String[] {".txt", ".html", ".log", ".rtf"};

    @Override
    public boolean accept(File file) {
        if(file.isDirectory()) return true;
        for (String extension : okFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "text files (*.txt, *.html, *.log, *.rtf)";
    }
}
