package JavaTextEditor;

import JavaTextEditor.ui.TextEditorUI;
import JavaTextEditor.ui.TextEditorArea;
import JavaTextEditor.util.IOManager;
import java.io.File;
import java.io.IOException;
import JavaTextEditor.Models.TextDocument;

/**
 * Created by ChrisIsKing on 4/27/16.
 */
public class JavaTextEditorController {
    private IOManager ioManager;
    private TextEditorUI textEditorUI;

    public JavaTextEditorController(TextEditorUI textEditorUI) {
        this.textEditorUI = textEditorUI;
        ioManager = new IOManager();
    }

    public void handleOpenTextDocument() {
        TextDocument doc = null;
        File f = textEditorUI.showOpenFileDialog();

        if(f != null) {
            try {
                String contents = ioManager.readTextFile(f);
                doc = new TextDocument(f.getName(), contents);
                textEditorUI.addTextEditorTab(doc);
            } catch (IOException e) {
                textEditorUI.showErrorDialog("Unable to open file");
            } catch (OutOfMemoryError err) {
                textEditorUI.showErrorDialog("Out of memory");
            }
        }
    }

    public void handleSaveTextDocument() {
        File f = null;

        //first get the active text area component..
        TextEditorArea textAreaComponent = textEditorUI.getSelectedTextArea();
        //then grab its Textdocument object
        TextDocument doc = textAreaComponent.getTextDocument();

        //if the document is a new document that has never been saved..we should show
        //the save file dialog..
        if(doc.getFileRef() == null) {
            f = textEditorUI.showSaveFileDialog();
        } else {
            //otherwise, we just go ahead and save the document without prompting..
            //since it is already on the file system..
            f = doc.getFileRef();
        }

        if(f != null) {

            try {
                ioManager.writeTextFile(f, doc.getContents());
                //update the file reference in the document
                doc.setFileRef(f);
                //update the textAreaComponent's TextDocument..the file ref
                //and file name could have changed..
                textAreaComponent.setTextDocument(doc);
                //update the filename in the tab of the ui
                textEditorUI.setActiveTabTitle(doc.getFileName());

            } catch(IOException ex) {
                textEditorUI.showErrorDialog("Problem saving file");
            }
        }
    }
}
